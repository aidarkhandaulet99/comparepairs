package com.example.comparepairs.controller;

import com.example.comparepairs.TutorialRepository;
import com.example.comparepairs.model.ComparePairsRequest;
import com.example.comparepairs.model.ComparePairsResponse;
import com.example.comparepairs.model.Tutorial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ComparePairsController {

    @Autowired
    TutorialRepository tutorialRepository;

    @GetMapping("result")
    public ComparePairsResponse getResult() {
        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(0);
        arrayList.add(45);
        arrayList.add(56);
        arrayList.add(999);
        arrayList.add(2);
        arrayList.add(24);
        arrayList.add(32);
        arrayList.add(10);
        arrayList.add(89);
        arrayList.add(6);

        List<Integer> arrayList2 = new ArrayList<>();
        arrayList2.add(23);
        arrayList2.add(2);
        arrayList2.add(3);
        arrayList2.add(65);
        arrayList2.add(23);
        arrayList2.add(5);
        arrayList2.add(963);
        arrayList2.add(111);
        arrayList2.add(852);
        arrayList2.add(8);

        List<Integer> arraylist3 = new ArrayList<>();


        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) >= arrayList2.get(i)) {
                arraylist3.add(arrayList.get(i));
            } else {
                arraylist3.add(arrayList2.get(i));
            }

        }
        ComparePairsResponse response = new ComparePairsResponse();
        response.setResult(arraylist3);

        return response;
    }

    @GetMapping("test")
    public List<Tutorial> getAll() {
        return tutorialRepository.getAll();
    }


    @PostMapping("result")
    public ComparePairsResponse setResult(@RequestBody ComparePairsRequest request) {
        List<Integer> arraylist3 = new ArrayList<>();

        for (int i = 0; i < request.getFirst().size(); i++) {
            if (request.getFirst().get(i) >= request.getSecond().get(i)) {
                arraylist3.add(request.getFirst().get(i));
            } else {
                arraylist3.add(request.getSecond().get(i));
            }

        }
        ComparePairsResponse response = new ComparePairsResponse();
        response.setResult(arraylist3);

        return response;
    }


}

//public class Compare {
//3. Дано 10 пар чисел. сравнить числа в каждой паре и напечатать большие из них.
//	public static void main (String[] args){
//		int[] firstArray = {5, 4, 12, 20, 1, 3, 15, 43, 9, 6};
//		int[] secondArray = {66, 5, 14, 21, 4, 9, 23, 47, 10, 11};
//		int [] thirdArray = new int [10];
//		for (int i = 0; i < firstArray.length; i++){
//			if(firstArray[i] >= secondArray[i] ) {
//				thirdArray[i] = firstArray[i];
//			} else {
//				thirdArray[i] = secondArray[i];
//			}
//		}
//		for (int i = 0; i < thirdArray.length; i++) {
//			System.out.println(thirdArray[i]);
//		}
//	}
//}
