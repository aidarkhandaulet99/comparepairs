package com.example.comparepairs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComparepairsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComparepairsApplication.class, args);
	}

}
