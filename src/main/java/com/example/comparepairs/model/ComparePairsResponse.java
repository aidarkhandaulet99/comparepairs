package com.example.comparepairs.model;

import java.util.List;

public class ComparePairsResponse {
   List<Integer> result;

    public List<Integer> getResult() {
        return result;
    }

    public void setResult(List<Integer> result) {
        this.result = result;
    }
}
