package com.example.comparepairs.model;

import java.util.List;

public class ComparePairsRequest {
    public ComparePairsRequest() {}
    public List<Integer> first;
    public List<Integer> second;

    public List<Integer> getSecond() {
        return second;
    }

    public void setSecond(List<Integer> second) {
        this.second = second;
    }

    public List<Integer> getFirst() {
        return first;
    }

    public void setFirst(List<Integer> first) {
        this.first = first;
    }
}
